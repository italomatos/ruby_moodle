== Ruby Moodle

##First of all, you need to enable Web Service plugin on Moodle

1. Log in with admin account, and go to Dashboard;
2. in the left side, there is "Site Administration" option;
3. Click on "Site Administration" -> Plugin -> Web Services -> Overall
4. Click on first item, "Enable Web Services", check "enable web services" option and save;
5. Go back to overall and click on "Enable protocols"; 
6. Enable "REST protocol" option and save;
7. Go back to overall and click on "Select a service"; 
8. there is "Moodle mobile web service" register there, click on "Edit";
9. Check if "enabled" option is enable, if not enabled check it and save. obs.: Here you will found service_name that you need to pass on MoodleService initialize. This field is "Short Name". In the next section you will use it;
10. You can create a specific user to use the Web service in: "Site Administration" -> Plugin -> Web Services -> Overall -> "Create a specific user", so, I recommend that you try first with admin user;

- Check all available methods on API: http://{your domain}/admin/webservice/documentation.php


After enable Web Service plugin on Moodle's server you will use the lib that I do

```
require 'moodle_client'

config = {
            :domain=>"http://moodle.toboxtv.com.br", 
            :username=>"admin", 
            :password=>"1t4L0matos-", 
            :service_name=>"moodle_mobile_app"
          }
moodle_service = MoodleService.new(config)
result = moodle_service.core_user_view_user_profile({:userid=>1})
puts result

```

Change domain, username and password

