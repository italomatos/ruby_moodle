require 'net/https'
require 'net/http'
require 'json'
require 'logger'
require 'uri'

class MoodleService

	attr_accessor :domain, :token, :username, :password, :service_name

	def initialize (args={})
		@domain = "#{args[:domain]}"

		if args.has_key? :token
			self.token = args[:token]
		elsif args.has_key? :username and args.has_key? :password and args.has_key? :service_name
			self.username = args[:username]
			self.password = args[:password]
			self.service_name = args[:service_name]

			result = self.request({:method=>"GET", :end_point=>"/login/token.php?username=#{self.username}&password=#{self.password}&service=#{self.service_name}"})
			if result['token']
				self.token = result['token']
			else
				raise ArgumentError, result
			end
		else
			raise ArgumentError, "You need to set token param or username and password"
		end
		
	end

protected
	def request(args={:method=>"POST"})

		logger = Logger.new(STDOUT)
		end_point = args[:end_point]

		if args[:params] and args[:params].size > 0 
			hash_params = args[:params].first
			_keys = hash_params.keys
			params_url = ""
			_keys.each{|t| params_url = params_url + "&#{t.to_s}=#{hash_params[t]}"}
			end_point = end_point + params_url
		end
		uri = URI(@domain + end_point)

		http = Net::HTTP.new uri.host, uri.port
		http.read_timeout = 80

		if args[:method] == "GET"
			request = Net::HTTP::Get.new(uri.request_uri)
		elsif args[:method] == "POST"
			request = Net::HTTP::Post.new(uri.request_uri)
		else
			logger.info("No method error!")
		end

		response = http.request(request)
		obj = JSON.parse(response.body)
		return obj
	end

	def method_missing(method_sym, *arguments, &block)
		end_point = "/webservice/rest/server.php?wstoken=#{self.token}&moodlewsrestformat=json&wsfunction=#{method_sym.to_s}"
		self.send('request', {:method=>"POST", :end_point => end_point, :params=>arguments})
	end
end


